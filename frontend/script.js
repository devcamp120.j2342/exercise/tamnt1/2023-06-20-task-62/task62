/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */

const gORDER_URL = "http://localhost:8080/ward-list";

//Hàm chính để load html hiển thị ra bảng
class Main {
    constructor() {
        $(document).ready(() => {
            this.vOrderList = new RenderPage()
            this.vOrderList.renderPage()

        })
    }

}
new Main()
/*** REGION 2 - vùng để render ra bảng*/
class RenderPage {
    constructor() {
        this.vApi = new CallApi()

    }

    //Hàm gọi api lấy danh sách đơn hàng
    _getOrderList() {
        this.vApi.onGetVoucherClick((paramOrder) => {
            console.log(paramOrder)
            this._createOrderTable(paramOrder)
        })
    }

    //Hàm tạo các thành phần của bảng
    _createOrderTable(paramOrder) {
        let stt = 1
        if ($.fn.DataTable.isDataTable('#table-order')) {
            $('#table-order').DataTable().destroy();
        }
        const vOrderTable = $("#table-order").DataTable({
            // Khai báo các cột của datatable
            "columns": [
                { "data": gCOL_NAME[gCOLUMN_ID.stt] },
                { "data": gCOL_NAME[gCOLUMN_ID.orderCode] },
                { "data": gCOL_NAME[gCOLUMN_ID.customerId] },
                { "data": gCOL_NAME[gCOLUMN_ID.productId] },
                { "data": gCOL_NAME[gCOLUMN_ID.pizzaSize] },
                { "data": gCOL_NAME[gCOLUMN_ID.pizzaType] },
                { "data": gCOL_NAME[gCOLUMN_ID.voucherCode] },
                { "data": gCOL_NAME[gCOLUMN_ID.price] },
                { "data": gCOL_NAME[gCOLUMN_ID.paid] },
        
            ],
            // Ghi đè nội dung của cột action
            "columnDefs": [
               
                {
                    targets: gCOLUMN_ID.stt,
                    render: function() {
                       return stt++
                    }
                }],

        });
        vOrderTable.clear() // xóa toàn bộ dữ liệu trong bảng
        vOrderTable.rows.add(paramOrder) // cập nhật dữ liệu cho bảng
        vOrderTable.draw()// hàm vẻ lại bảng
    }

    // Hàm sẽ được gọi ở class Main 
    renderPage() {
        this._getOrderList()
    }
}

/*** REGION 3 - vùng để gọi lên cơ sở dữ liệu lấy đa ta về*/
class CallApi {
    constructor() {

    }
    onGetVoucherClick(paramCallbackFn) {
     
        $.ajax({
            url: gORDER_URL,
            method: "GET",
            success: function (data) {
                paramCallbackFn(data)
            },
            error: function (jqXHR, textStatus, errorThrown) {
               
            }
        });
    }

}
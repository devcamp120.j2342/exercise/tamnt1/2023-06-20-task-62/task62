package com.devcamp.provinceapi.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.devcamp.provinceapi.models.Province;
import com.devcamp.provinceapi.repository.ProvinceRepository;

@Service
public class ProvinceService {
    private final ProvinceRepository provinceRepository;

    public ProvinceService(ProvinceRepository provinceRepository) {
        this.provinceRepository = provinceRepository;
    }

    public List<Province> getProvince(String page, String size) {
        int pageNumber = Integer.parseInt(page);
        int pageSize = Integer.parseInt(size);

        Pageable pageable = PageRequest.of(pageNumber, pageSize);

        Page<Province> provincePage = provinceRepository.findAll(pageable);
        List<Province> provinceList = provincePage.getContent();
        return provinceList;
    }

}

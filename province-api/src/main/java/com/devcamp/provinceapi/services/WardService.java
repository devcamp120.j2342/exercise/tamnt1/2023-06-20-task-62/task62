package com.devcamp.provinceapi.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.devcamp.provinceapi.models.District;
import com.devcamp.provinceapi.models.Ward;
import com.devcamp.provinceapi.repository.WardRepository;

@Service
public class WardService {
    private final WardRepository wardRepository;

    public WardService(WardRepository wardRepository) {
        this.wardRepository = wardRepository;
    }

    public List<Ward> getWards(String page, String size) {
        int pageNumber = Integer.parseInt(page);
        int pageSize = Integer.parseInt(size);

        Pageable pageable = PageRequest.of(pageNumber, pageSize);

        Page<Ward> wardPage = wardRepository.findAll(pageable);
        List<Ward> wardList = wardPage.getContent();
        return wardList;
    }

    public List<Ward> getWardByDistrict(String page, String size, String id) {
        int pageNumber = Integer.parseInt(page);
        int pageSize = Integer.parseInt(size);
        int districtId = Integer.parseInt(id);
        Pageable pageable = PageRequest.of(pageNumber, pageSize);

        Page<Ward> wardPage = wardRepository.getWardByDistrictId(districtId, pageable);
        List<Ward> wardList = wardPage.getContent();
        return wardList;
    }
}

package com.devcamp.provinceapi.services;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.devcamp.provinceapi.models.District;
import com.devcamp.provinceapi.repository.DistrictRepository;

@Service
public class DistrictService {
    private final DistrictRepository districtRepository;

    public DistrictService(DistrictRepository districtRepository) {
        this.districtRepository = districtRepository;
    }

    public List<District> getDistricts(String page, String size) {
        int pageNumber = Integer.parseInt(page);
        int pageSize = Integer.parseInt(size);
        Pageable pageable = PageRequest.of(pageNumber, pageSize);

        Page<District> districtPage = districtRepository.findAll(pageable);
        List<District> districtList = districtPage.getContent();
        return districtList;
    }

    public List<District> getDistrictbyProvince(String page, String size, String id) {
        int pageNumber = Integer.parseInt(page);
        int pageSize = Integer.parseInt(size);
        int provinceId = Integer.parseInt(id);
        Pageable pageable = PageRequest.of(pageNumber, pageSize);

        Page<District> districtPage = districtRepository.getDistrictsByProvinceId(provinceId, pageable);
        List<District> districtList = districtPage.getContent();
        return districtList;
    }
}

package com.devcamp.provinceapi.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.provinceapi.models.District;

public interface DistrictRepository extends JpaRepository<District, Integer> {
    @Query("SELECT d FROM District d WHERE d.province.id = :provinceId")
    public Page<District> getDistrictsByProvinceId(@Param("provinceId") int provinceId, Pageable pageable);

}

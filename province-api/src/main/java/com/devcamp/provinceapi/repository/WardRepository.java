package com.devcamp.provinceapi.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.provinceapi.models.Ward;

public interface WardRepository extends JpaRepository<Ward, Integer> {
    @Query("SELECT d from Ward d WHERE d.district.id = :districtId")
    public Page<Ward> getWardByDistrictId(@Param("districtId") int districtId, Pageable pageable);

}

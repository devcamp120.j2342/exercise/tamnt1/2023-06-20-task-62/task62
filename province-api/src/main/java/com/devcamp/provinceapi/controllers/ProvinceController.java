package com.devcamp.provinceapi.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.provinceapi.models.Province;
import com.devcamp.provinceapi.services.ProvinceService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class ProvinceController {
    @Autowired
    ProvinceService provinceService;

    @GetMapping("/province-list")
    public List<Province> getProvinceList(@RequestParam(value = "page", defaultValue = "0") String page,
            @RequestParam(value = "size", defaultValue = "5") String size) {
        return provinceService.getProvince(page, size);
    }

}

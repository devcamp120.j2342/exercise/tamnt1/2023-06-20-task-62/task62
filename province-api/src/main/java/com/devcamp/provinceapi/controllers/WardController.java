package com.devcamp.provinceapi.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.provinceapi.models.District;
import com.devcamp.provinceapi.models.Ward;
import com.devcamp.provinceapi.services.WardService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class WardController {
    @Autowired
    WardService wardService;

    @GetMapping("/ward-list")
    public List<Ward> getDistrictList(@RequestParam(value = "page", defaultValue = "0") String page,
            @RequestParam(value = "size", defaultValue = "5") String size) {
        return wardService.getWards(page, size);
    }

    @GetMapping("/ward-by-districtId/{id}")
    public List<Ward> getWardListByDistrictId(@RequestParam(value = "page", defaultValue = "0") String page,
            @RequestParam(value = "size", defaultValue = "5") String size, @PathVariable("id") String id) {
        return wardService.getWardByDistrict(page, size, id);
    }
}

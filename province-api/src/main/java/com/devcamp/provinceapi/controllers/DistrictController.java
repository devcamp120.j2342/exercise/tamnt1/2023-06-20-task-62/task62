package com.devcamp.provinceapi.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.provinceapi.models.District;
import com.devcamp.provinceapi.services.DistrictService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class DistrictController {
    @Autowired
    DistrictService districtService;

    @GetMapping("/district-list")
    public List<District> getDistrictList(@RequestParam(value = "page", defaultValue = "0") String page,
            @RequestParam(value = "size", defaultValue = "5") String size) {
        return districtService.getDistricts(page, size);
    }

    @GetMapping("/district-by-provinceId/{id}")
    public List<District> getDistrictListByProvinceId(@RequestParam(value = "page", defaultValue = "0") String page,
            @RequestParam(value = "size", defaultValue = "5") String size, @PathVariable("id") String id) {
        return districtService.getDistrictbyProvince(page, size, id);
    }
}
